# Copyright 2008-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'PyQt4-4.4.4-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

myexparam sip_version

require pypi
require python [ blacklist=2 multiunpack=true ]

SUMMARY="PyQt5 is a set of Python bindings for the Qt5 toolkit"
DESCRIPTION="
PyQt is a set of Python bindings for the Qt application framework and runs
on all platforms supported by Qt including Windows, MacOS/X and Linux. The
bindings are implemented as a set of Python modules and contain over 300
classes and over 6,000 functions and methods.
"
BASE_URI="https://www.riverbankcomputing.com"
HOMEPAGE="${BASE_URI}/software/pyqt/download5/"
DOWNLOADS="https://pypi.python.org/packages/source/P/${PN}/${PNV}.tar.gz"

UPSTREAM_RELEASE_NOTES="${BASE_URI}/news/pyqt-$(ever delete_all)"
UPSTREAM_DOCUMENTATION="${BASE_URI}/static/Docs/${PN}/ [[ lang = en ]]"

SLOT="0"
LICENCES="|| ( GPL-2 GPL-3 )"
MYOPTIONS="dbus debug
    bluetooth     [[ description = [ Build bindings for QtBluetooth & QtNfc ] ]]
    multimedia    [[ description = [ Build bindings for QtMultimedia ] ]]
    positioning   [[ description = [ Build bindings for QtPositioning ] ]]
    quick3d       [[ description = [ Build bindings for QtQuick3D ] ]]
    remoteobjects [[ description = [ Build bindings for QtRemoteObjects ] ]]
    sensors       [[ description = [ Build bindings for QtSensors ] ]]
    serialport    [[ description = [ Build bindings for QtSerialPort ] ]]
    sql           [[ description = [ Build bindings for QtSql ] ]]
    texttospeech  [[ description = [ Build bindings for QtTextToSpeech ] ]]
    webchannel    [[ description = [ Build bindings for QtWebChannel ] ]]
    webkit        [[ description = [ Build bindings for QtWebKit ] ]]
    websockets    [[ description = [ Build bindings for QWebSockets ] ]]
    x11extras     [[ description = [ Build bindings for QtX11Extras ] ]]
"

DEPENDENCIES="
    build:
        dev-python/PyQt5-sip[>=12.13][python_abis:*(-)?]
        dev-python/PyQt-builder[>=1.14.1][python_abis:*(-)?]
    build+run:
        dev-python/sip:0[>=$(exparam sip_version)][python_abis:*(-)?]
        x11-libs/qtbase:5
        x11-libs/qtdeclarative:5
        x11-libs/qtsvg:5
        x11-libs/qttools:5 [[ note = [ QtHelp ] ]]
        x11-libs/qtxmlpatterns:5
        bluetooth? ( x11-libs/qtconnectivity:5 )
        dbus? (
            dev-python/dbus-python[python_abis:*(-)?]
            sys-apps/dbus
        )
        multimedia? ( x11-libs/qtmultimedia:5 )
        positioning? ( x11-libs/qtlocation:5 )
        quick3d? ( x11-libs/qtquick3d:5 )
        remoteobjects? ( x11-libs/qtremoteobjects:5 )
        sensors? ( x11-libs/qtsensors:5 )
        serialport? ( x11-libs/qtserialport:5 )
        sql? ( x11-libs/qtbase:5[sql] )
        texttospeech? ( x11-libs/qtspeech:5 )
        webchannel? ( x11-libs/qtwebchannel:5 )
        webkit? ( x11-libs/qtwebkit:5 )
        websockets? ( x11-libs/qtwebsockets:5 )
        x11extras? ( x11-libs/qtx11extras:5 )
"

DEFAULT_SRC_INSTALL_PARAMS=( INSTALL_ROOT="${IMAGE}" )

pyqt_enable() {
    local flag=${1,,} module=${2:-Qt${1}}

    if option "${flag}" ; then
        echo "--enable=${module}"
    fi
}

# Unused modules:
# QAxContainer: Qt's ActiveX and COM support (qtactiveqt)
# QtAdndroidExtras, QtMacExtras, QtWinExtras: Platform specific components for
#    Android, Mac OS, Windows
PYQT_CONFIGURE_OPTION_ENABLES=(
    Bluetooth
    "bluetooth QtNfc"
    DBus
    Multimedia
    "multimedia QtMultimediaWidgets"
    Quick3D
    "remoteobjects QtRemoteObjects"
    Sensors
    SerialPort
    Sql
    "texttospeech QtTextToSpeech"
    "webchannel QtWebChannel"
    WebKit
    "webkit QtWebKitWidgets"
    "websockets QtWebSockets"
    X11Extras
)
PYQT_CONFIGURE_ALWAYS_ENABLES=(
    Qt
    QtCore
    QtGui
    QtHelp
    QtNetwork
    QtOpenGL
    QtPrintSupport
    QtQml
    QtQuick
    QtQuickWidgets
    QtSvg
    QtTest
    QtWidgets
    QtXml
    QtXmlPatterns
    _QOpenGLFunctions_2_0
    _QOpenGLFunctions_2_1
    _QOpenGLFunctions_4_1_Core
)
PYQT_CONFIGURE_PARAMS=(
        # Disable PyQt API file for QScintilla
         --api-dir /usr/share/qt/qsci/api/python
        # Disable QtDesigner support. Enable with: --enable=QtDesigner --enable=uic
        --no-designer-plugin
)

prepare_one_multibuild() {
    python_prepare_one_multibuild

    if option dbus; then
        edo sed -e "s:'pkg-config':'$(exhost --tool-prefix)pkg-config':" \
                -i project.py
    else
        edo sed -e '/pkg-config.*dbus-1/s/sout.*$/return # DBus support disabled/' -i configure.py
    fi

    # When system python is set to 2.6 python_bytecompile() can't process
    # source files that use python 3 syntax. See Gentoo bug #274499.
    [[ $(python_get_abi) == 3.* ]] && edo rm -rf pyuic/uic/port_v2
}

configure_one_multibuild() {
    edo ${PYTHON} /usr/$(exhost --target)/bin/sip-build \
        $(option debug && echo '--debug') \
        $(option debug && echo '--qml-debug') \
        --confirm-license \
        --no-make \
        --qmake /usr/$(exhost --target)/lib/qt5/bin/qmake \
        --target-dir $(python_get_sitedir) \
        --verbose \
        "${PYQT_CONFIGURE_PARAMS[@]}" \
        $(for s in ${PYQT_CONFIGURE_ALWAYS_ENABLES[@]} ; do echo "--enable=${s}" ; done) \
        $(for s in "${PYQT_CONFIGURE_OPTION_ENABLES[@]}" ; do pyqt_enable ${s} ; done)

    edo find "${WORK}" -name Makefile | xargs sed -i "/^\tstrip /d"
}

compile_one_multibuild() {
    edo cd build
    emake
}

install_one_multibuild() {
    edo pushd build
    emake -j1 INSTALL_ROOT="${IMAGE}" install
    python_bytecompile
    edo popd

    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)/bin
    edo mv "${IMAGE}"/usr/bin/* "${IMAGE}"/usr/$(exhost --target)/bin
    edo rmdir "${IMAGE}"/usr/bin

    insinto /usr/share/doc/${PNVR}
    doins -r examples
}

