# Copyright 2019-2022 Ridai Govinda Pombo <ridai.govinda@keemail.me>
# Distributed under the terms of the GNU General Public License v2
# Based in 'PyQtWebEngine.exlib', which is:
#     Copyright 2019-2020 Heiko Becker <heirecka@exherbo.org>

myexparam sip_version

MY_PN="${PN/PyQt6Web/PyQt6-Web}"
MY_PNV="${MY_PN/-/_}-${PV}"

require pypi
require python [ blacklist=2 multiunpack=true work=${MY_PNV} ]

SUMMARY="PyQt6WebEngine is a set of Python bindings for the Qt WebEngine framework"
DESCRIPTION="The framework provides the ability to embed web content in
applications and is based on the Chrome browser. The bindings sit on top of
PyQt6 and are implemented as three separate modules corresponding to the
different libraries that make up the framework."

BASE_URI="https://www.riverbankcomputing.com"
HOMEPAGE="${BASE_URI}/software/pyqt/download/"
DOWNLOADS="https://pypi.python.org/packages/source/P/${MY_PN}/${MY_PNV}.tar.gz"

SLOT="0"
LICENCES="GPL-3"
MYOPTIONS="debug"

DEPENDENCIES="
    build:
        dev-python/PyQt-builder[>=1.11&<2][python_abis:*(-)?]
    build+run:
        dev-python/PyQt6[>=${PV}][python_abis:*(-)?][webchannel]
        dev-python/sip:0[>=$(exparam sip_version)][python_abis:*(-)?]
        x11-libs/qtbase:6
        x11-libs/qtwebengine:6
"

DEFAULT_SRC_INSTALL_PARAMS=( INSTALL_ROOT="${IMAGE}" )

prepare_one_multibuild() {
    python_prepare_one_multibuild

    # When system python is set to 2.6 python_bytecompile() can't process
    # source files that use python 3 syntax. See Gentoo bug #274499.
    [[ $(python_get_abi) == 3.* ]] && edo rm -rf pyuic/uic/port_v2
}

configure_one_multibuild() {
    local myparams=(
        --no-make
        --qmake /usr/$(exhost --target)/lib/qt6/bin/qmake
        --target-dir $(python_get_sitedir) \
        --verbose
        $(option debug && echo '--debug')
    )

    edo ${PYTHON} /usr/$(exhost --target)/bin/sip-build "${myparams[@]}"
}

compile_one_multibuild() {
    edo cd build
    emake
}

install_one_multibuild() {
    edo pushd build

    default
    python_bytecompile

    edo popd

    emagicdocs
}

