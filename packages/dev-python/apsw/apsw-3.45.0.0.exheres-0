# Copyright 2013-2020 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV=${PV/-p/-r}
MY_PNV=${PN}-${MY_PV}

require github [ user=rogerbinns tag=${MY_PV} ]
require setup-py [ import=setuptools blacklist=2 work=${MY_PNV} ]

SUMMARY="APSW is a Python wrapper for SQLite"

# "Alternatively you may strike the license above and use it under any OSI
# approved open source license such as those listed at
# https://opensource.org/licenses/alphabetical"
LICENCES="ZLIB"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-db/sqlite:3[>=$(ever range -3)]
"

SETUP_PY_SRC_COMPILE_PARAMS=(
    # calibre needs this, or it fails to connect to its sqlite db with:
    # "AttributeError: 'Connection' object has no attribute 'enableloadextension'"
    --enable=load_extension
    build_ext --use-system-sqlite-config
)

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # disable test failing because of sandbox
    edo sed \
        -e 's:"Check Shell functionality":return:g' \
        -i apsw/tests.py
}

test_one_multibuild() {
    edo "${PYTHON}" setup.py build_test_extension
    PYTHONPATH=$(ls -d build/lib*) edo "${PYTHON}" apsw/tests.py -v
}

