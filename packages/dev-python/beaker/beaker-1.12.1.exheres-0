# Copyright 2008, 2009 Ali Polatel
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'beaker-0.9.4.ebuild' from Gentoo, which is:
#   Copyright 1999-2008 Gentoo Foundation

require pypi [ pnv=${PNV^} ]
require setup-py [ blacklist=2 import=setuptools work=${PNV^} test=pytest ]

SUMMARY="A simple WSGI middleware to use the Myghty Container API"
DESCRIPTION="
Beaker is a caching library that includes Session and Cache objects built on
Myghty's Container API used in MyghtyUtils. WSGI middleware is also included to
manage Session objects and signed cookies.
Beaker caching is implemented with namespaces allowing one to store not only any
Python data that can be pickled, but also multiple versions of it by using
multiple keys for a single piece of data under a namespace.
"
HOMEPAGE+=" https://github.com/bbangert/beaker"

UPSTREAM_DOCUMENTATION="
    https://beaker.readthedocs.io/en/latest/ [[ lang = en ]]
"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    test:
        dev-python/mock[python_abis:*(-)?]
        dev-python/pycryptodome[python_abis:*(-)?]
"

# Pulls in quite a number of deps in addition to the ones above, e.g.
# pymongo3, which we don't have
RESTRICT="test"

