# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=di ] py-pep517 [ backend=setuptools ]

SUMMARY="Setuptools extension for CalVer package versions"
DESCRIPTION="
The calver package is a setuptools extension for automatically defining your
Python package version as a calendar version."

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    test:
        dev-python/pretend[python_abis:*(-)?]
    suggestion:
        dev-python/pytest[python_abis:*(-)?]
"

test_one_multibuild() {
    # Avoid a dependency cycle between pytest -> iniconfig -> hatchling ->
    # trove-classifiers -> calver -> pytest
    if has_version dev-python/pytest[python_abis:$(python_get_abi)] ; then
        py-pep517_run_tests_pytest
    else
        ewarn "dev-python/pytest is not yet installed, skipping tests"
    fi
}

