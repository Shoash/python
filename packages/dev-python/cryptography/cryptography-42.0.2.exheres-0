# Copyright 2014 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require cargo [ rust_minimum_version=1.56.0 ]
require pypi py-pep517 [ backend=setuptools test=pytest ]
require flag-o-matic

SUMMARY="Cryptographic recipes and primitives for Python developers"
DESCRIPTION="
cryptography is a Python library which exposes cryptographic recipes and
primitives. Our goal is for it to be your 'cryptographic standard library'.

cryptography includes both high level recipes, and low level interfaces
to common cryptographic algorithms such as symmetric ciphers, message
digests and key derivation functions.
"
HOMEPAGE+=" https://cryptography.io"

LICENCES="|| ( Apache-2.0 BSD-3 ) PSF"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-python/setuptools-rust[>=1.7.0][python_abis:*(-)?]
    build+run:
        dev-python/cffi[>=1.12][python_abis:*(-)?]
        providers:libressl? ( dev-libs/libressl:=[>=2.9.1] )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.0] )
    test:
        dev-python/certifi[python_abis:*(-)?]
        dev-python/cryptography-vectors[~${PV}][python_abis:*(-)?]
        dev-python/pretend[python_abis:*(-)?]
        dev-python/pytest[>=6.2.0][python_abis:*(-)?]
        dev-python/pytest-benchmark[python_abis:*(-)?]
        dev-python/pytest-cov[python_abis:*(-)?]
        dev-python/pytest-xdist[python_abis:*(-)?]
"

prepare_one_multibuild() {
    py-pep517_prepare_one_multibuild

    edo pushd src/rust
    ecargo_fetch
    edo popd
}

configure_one_multibuild() {
    # "ImportError: ...site-packages/cryptography/hazmat/bindings/_rust.abi3.so:
    # undefined symbol: PyInit__openssl"
    filter-flags -flto

    py-pep517_prepare_one_multibuild
}

