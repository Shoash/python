# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Yelp tag=v${PV} ]
require setup-py [ import=setuptools blacklist='2' test=pytest ]

SUMMARY="Bind to an ephemeral port, force it into the TIME_WAIT state, and unbind it"

DESCRIPTION="Sometimes you need a networked program to bind to a port that
can't be hard-coded. Generally this is when you want to run several of them in
parallel; if they all bind to port 8080, only one of them can succeed.
The usual solution is the \"port 0 trick\". If you bind to port 0, your kernel
will find some arbitrary high-numbered port that's unused and bind to that.
Afterward you can query the actual port that was bound to if you need to use
the port number elsewhere. However, there are cases where the port 0 trick
won't work.
ephemeral-port-reserve provides an implementation of the port 0 trick which
is reliable and race-free."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

