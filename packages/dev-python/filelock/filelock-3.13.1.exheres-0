# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi
require py-pep517 [ backend=hatchling backend_version_spec="[>=1.18]" test=pytest ]

SUMMARY="A platform independent file lock"
DESCRIPTION="
It contains a single module, which implements a platform independent file lock
in Python.
The lock includes a lock counter and is thread safe. This means, when locking
the same lock object twice, it will not block.
"

LICENCES="public-domain"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/hatch_vcs[>=0.3][python_abis:*(-)?]
    build+run:
        python_abis:3.8? ( dev-python/typing-extensions[>=4.8][python_abis:3.8] )
        python_abis:3.9? ( dev-python/typing-extensions[>=4.8][python_abis:3.9] )
        python_abis:3.10? ( dev-python/typing-extensions[>=4.8][python_abis:3.10] )
    test:
        dev-python/coverage[>=4][python_abis:*(-)?]
        dev-python/pytest-cov[>=4.1][python_abis:*(-)?]
        dev-python/pytest-mock[>=3.12][python_abis:*(-)?]
        dev-python/pytest-timeout[>=2.2][python_abis:*(-)?]
"

