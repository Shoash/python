# Copyright 2021 Arnaud Lefebvre <a.lefebvre@outlook.fr>
# Distributed under the terms of the GNU General Public License v2

require alternatives pypi setup-py [ import=setuptools blacklist="2" has_bin=true ]

SUMMARY="A WSGI object-dispatching web framework"
HOMEPAGE="https://www.pecanpy.org/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    run:
        dev-python/logutils[>=0.3][python_abis:*(-)?]
        dev-python/Mako[>=0.4.0][python_abis:*(-)?]
        dev-python/WebOb[>=1.8][python_abis:*(-)?]
    test:
        dev-python/Genshi[>=0.7][python_abis:*(-)?]
        dev-python/Jinja2[python_abis:*(-)?]
        dev-python/pep8[python_abis:*(-)?]
        dev-python/SQLAlchemy[python_abis:*(-)?]
        dev-python/virtualenv[python_abis:*(-)?]
        dev-python/WebTest[>=1.3.1][python_abis:*(-)?]
        www-servers/gunicorn[python_abis:*(-)?]
        www-servers/uwsgi[python_abis:*(-)?]
"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # Only required to support python 3.5, which we don't care about anymore
    edo sed -e "s/Jinja2<3/Jinja2/" -i test-requirements.txt
}

install_one_multibuild(){
    setup-py_install_one_multibuild

    alternatives_for \
        ${PN}_python ${MULTIBUILD_TARGET} ${MULTIBUILD_TARGET} \
        /usr/$(exhost --target)/bin/${PN} ${PN}${MULTIBUILD_TARGET}
}

