# Copyright 2022 Stanislav Sauvin <stanislav.sauvin@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN/-/_}
MY_PNV=${MY_PN}-${PV}

require pypi py-pep517 [ backend=self-hosted test=pytest work=${MY_PNV} ]

SUMMARY="Poetry PEP 517 Build Backend"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/fastjsonschema[>=2.18.1][python_abis:*(-)?]
        dev-python/lark[>=1.1.8][python_abis:*(-)?]
        dev-python/packaging[>=23.2][python_abis:*(-)?]
        python_abis:3.8? ( dev-python/tomli[>=2.0.1][python_abis:3.8] )
        python_abis:3.9? ( dev-python/tomli[>=2.0.1][python_abis:3.9] )
        python_abis:3.10? ( dev-python/tomli[>=2.0.1][python_abis:3.10] )
    test:
        dev-python/pyparsing[>=3.0.9][python_abis:*(-)?]
        dev-python/pytest-mock[>=3.10][python_abis:*(-)?]
        dev-python/tomli-w[>=0.11.6][python_abis:*(-)?]
        dev-python/virtualenv[>=20.21][python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/001-unvendor.patch
)

PYTEST_SKIP=("_pep_561_")
PYTEST_PARAMS=( --ignore=tests/integration/test_pep517.py )

prepare_one_multibuild() {
    edo rm -rf src/poetry/core/_vendor
    py-pep517_prepare_one_multibuild
}

