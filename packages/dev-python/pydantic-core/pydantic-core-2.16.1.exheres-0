# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN/-/_}
MY_PNV=${MY_PN}-${PV}

require cargo pypi
require py-pep517 [ backend=maturin backend_version_spec="[>=1&<2]" test=pytest work=${MY_PNV} ]

SUMMARY="Core validation logic for pydantic written in rust"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/typing-extensions[>=4.6.0][python_abis:*(-)?]
    test:
        dev-python/dirty-equals[>=0.6.0][python_abis:*(-)?]
        dev-python/hypothesis[>=0.6.0][python_abis:*(-)?]
        dev-python/pytest-benchmark[python_abis:*(-)?]
        dev-python/pytest-mock[>=3.11.1][python_abis:*(-)?]
        dev-python/pytest-timeout[>=2.1.0][python_abis:*(-)?]
        dev-python/pytz[>=0.6.0][python_abis:*(-)?]
        python_abis:3.8? ( dev-python/exceptiongroup[>=1.1][python_abis:3.8] )
        python_abis:3.9? ( dev-python/exceptiongroup[>=1.1][python_abis:3.9] )
        python_abis:3.10? ( dev-python/exceptiongroup[>=1.1][python_abis:3.10] )
"
# NOTE: There are some more optional test dependencies we don't have, e.g.
# pandas

prepare_one_multibuild() {
    py-pep517_prepare_one_multibuild

    ecargo_fetch
}

