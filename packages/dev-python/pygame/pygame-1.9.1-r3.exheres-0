# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pygame-1.8.1-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require bitbucket
require setup-py [ import=distutils work="${PNV}release" ]

SUMMARY="Python extension modules designed for writing games on top of SDL"
DESCRIPTION="
Pygame is a set of Python modules designed for writing games. Pygame adds
functionality on top of the excellent SDL library. This allows you to create
fully featured games and multimedia programs in the Python language. Pygame is
highly portable and runs on nearly every platform and operating system.
"
HOMEPAGE="http://www.pygame.org/"
DOWNLOADS="http://www.pygame.org/ftp/${PNV}release.tar.gz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc
    portmidi [[ description = [ Enable support for PortMidi real-time MIDI input/output library ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-python/numpy[python_abis:*(-)?]
        media-libs/SDL:0[>=1.2][X]
        media-libs/SDL_image:1
        media-libs/SDL_mixer:0
        media-libs/SDL_ttf:0
        media-libs/libpng:=
        media-libs/smpeg:0[>=0.4.3]
        x11-libs/libX11
        portmidi? ( media-libs/portmidi )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}docs/ [[ lang = en ]]"

# Due to pygame's nature as a multimedia framework, many of the included tests
# simply aren't feasible under Paludis for hardware reasons (e.g. some require
# DISPLAY to be set, some require *write* access to devices like /dev/dsp,
# framebuffer, etc.), and others throw errors too numerous to allow maintaining
# workarounds to be practical. Disable tests, but install them and notify the
# user so they can run them manually if they wish.
RESTRICT="test"

DEFAULT_SRC_PREPARE_PATCHES=(
    -p0 "${FILES}"/disable-config-prompt.patch
    -p1 "${FILES}"/${PNV}-remove-v4l1.patch
    -p1 "${FILES}"/${PNV}-fixes-for-python3.2.patch
        "${FILES}"/${PNV}-porttime.patch
)
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( WHATSNEW )

setup-py_configure_one_multibuild() {
    # Ugly but nothing else would work.
    edo ${PYTHON} -B config.py -auto

    # Disable automagic portmidi dependency.
    option portmidi || edo sed -e "/^pypm .*SDL/d" -i Setup
}

src_install() {
    setup-py_src_install

    if option doc ; then
        edo mv docs html
        dodoc -r html
    fi
}

pkg_postinst() {
    elog "Unit tests have been installed to:"
    elog "    $(python_get_sitedir)/${PN}/tests/"
}

