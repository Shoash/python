# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN="${PN/-/_}"
MY_PNV="${MY_PN}-${PV}"

require pypi py-pep517 [ backend=flit_core work=${MY_PNV} test=pytest ]

SUMMARY="A low-level library for calling build-backends"
DESCRIPTION="
This is a low-level library for calling build-backends in pyproject.toml-based
project. It provides the basic functionality to help write tooling that
generates distribution files from Python projects.
If you want a tool that builds Python packages, you'll want to use build
instead. This is an underlying piece for pip, build and other \"build
frontends\" use to call \"build backends\" within them."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        python_abis:3.8? ( dev-python/tomli[python_abis:3.8] )
        python_abis:3.9? ( dev-python/tomli[python_abis:3.9] )
        python_abis:3.10? ( dev-python/tomli[python_abis:3.10] )
    test:
        dev-python/setuptools[>=30][python_abis:*(-)?]
        dev-python/testpath[python_abis:*(-)?]
"

