# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=setuptools test=pytest ]

SUMMARY="Services plugin for pytest testing framework"
DESCRIPTION="
The plugin provides a set of fixtures and utility functions to start service
processes for your tests with pytest."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/setuptools_scm[>=3.4.1][python_abis:*(-)?]
    build+run:
        dev-python/psutil[python_abis:*(-)?]
        dev-python/pytest[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        dev-python/zc-lockfile[python_abis:*(-)?]
"

# Unwritten deps: mysqlclient, pylibmc
RESTRICT="test"

prepare_one_multibuild() {
    pyp-p517_prepare_one_multibuild

    # Avoid pytest-pop8
    edo sed -e '/pep8maxlinelength=/d' -i tox.ini
}

