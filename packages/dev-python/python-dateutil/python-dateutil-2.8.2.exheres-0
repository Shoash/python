# Copyright 2009 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'python-dateutil-1.4.1-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require pypi setup-py [ import=setuptools test=pytest ]
require utf8-locale

SUMMARY="Extensions to the standard Python datetime module"
DESCRIPTION="
The dateutil module provides powerful extensions to the
datetime module available in the Python standard library.
"

UPSTREAM_DOCUMENTATION="https://dateutil.readthedocs.io"

LICENCES="|| ( Apache-2.0 BSD-3 )"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/setuptools_scm:0[python_abis:*(-)?]
    build+run:
        dev-python/six[>=1.5][python_abis:*(-)?]
        sys-libs/timezone-data
    test:
        dev-python/coverage[python_abis:*(-)?]
        dev-python/hypothesis[>=3.30][python_abis:*(-)?]
        dev-python/pytest-cov[>=2.0.0][python_abis:*(-)?]
        python_abis:2.7? ( dev-python/mock[python_abis:2.7] )
    post:
        dev-python/freezegun[python_abis:*(-)?]  [[
            note = [ Test dependency, moved here to break cycle ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Remove-deprecated-pytest.warns-None-from-test_intern.patch
)

pkg_setup() {
    # Required for tests.
    require_utf8_locale
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    # provided by sys-libs/timezone-data
    edo rm -f "${IMAGE}"/usr/$(exhost --target)/lib/python*/site-packages/dateutil/zoneinfo/*.tar.*
}

test_one_multibuild() {
    if has_version dev-python/freezegun[python_abis:$(python_get_abi)]; then
        setup-py_test_one_multibuild
    else
        ewarn "Test dependency are not installed yet - skipping tests"
    fi
}

