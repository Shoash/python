# Copyright 2011 NAKAMURA Yoshitaka
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ blacklist=2 import=setuptools test=pytest ]

SUMMARY="Python bindings for 0MQ"
HOMEPAGE+=" https://zeromq.org/languages/python/"

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-python/Cython[>=0.29][python_abis:*(-)?]
        net-libs/zeromq[>=3.2]
    test:
        dev-python/tornado[python_abis:*(-)?]
        python_abis:3.8? ( dev-python/gevent[python_abis:3.8] )
        python_abis:3.9? ( dev-python/gevent[python_abis:3.9] )
        python_abis:3.10? ( dev-python/gevent[python_abis:3.10] )
"

PYTEST_PARAMS=(
    # TypeError: _FlakyPlugin._make_test_flaky() got an unexpected keyword
    # argument 'reruns', but upstream claims it doesn't use flaky
    # https://github.com/zeromq/pyzmq/issues/1802
    -p no:flaky
    # test_client_server: "zmq.error.Again: Resource temporarily unavailable"
    #     but not sydbox violation
    # test_radio_dish: "zmq.error.ZMQError: Invalid argument"
    -k "not test_client_server and not test_radio_dish"
    # mypy_tests is not part of the tarball, but needed for the test
    --ignore zmq/tests/test_mypy.py
)

test_one_multibuild() {
    local local_port_range="32768-60999"
    local whitelist=(
        "--connect LOCAL@${local_port_range}"
        "--connect LOCAL@2222"
        "--connect LOCAL@3333"
        "--connect LOCAL@4444"
        "--connect LOCAL@5555"
        "--connect LOCAL@7777"
        "inet:0.0.0.0@0"
        "inet:0.0.0.0@2222"
        "inet:0.0.0.0@3333"
        "inet:0.0.0.0@4444"
        "inet:0.0.0.0@5555"
        "inet:0.0.0.0@7777"
        "unix:/foo/bar"
    )

    for addr in "${whitelist[@]}"; do
        esandbox allow_net $addr
    done

    edo "${PYTHON}" -B setup.py build_ext --inplace
    setup-py_test_one_multibuild

    for addr in "${whitelist[@]}"; do
        esandbox disallow_net $addr
    done
}

