# Copyright 2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=hatchling test=pytest ]

SUMMARY="JSON Referencing + Python"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/hatch_vcs[python_abis:*(-)?]
    build+run:
        dev-python/attrs[>=22.2.0][python_abis:*(-)?]
        dev-python/rpds-py[>=0.7.0][python_abis:*(-)?]
    test:
        dev-python/jsonschema[python_abis:*(-)?]
        dev-python/pytest-subtests[python_abis:*(-)?]
"

# Do not collect tests from our installed package, tests shouldn't be installed
# but referencing seems to do it anyway.
PYTEST_PARAMS=("--ignore=pep517_tests_${PN}")

test_one_multibuild() {
    # Avoid dependency cycle between referencing & jsonschema
    if has_version dev-python/jsonschema[python_abis:$(python_get_abi)] ; then
        py-pep517_run_tests_pytest
    else
        ewarn "dev-python/jsonschema is not yet installed, skipping tests"
    fi
}

