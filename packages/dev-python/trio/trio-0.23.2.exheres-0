# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=setuptools test=pytest ]

SUMMARY="A friendly Python library for async concurrency and I/O"
DESCRIPTION="
Like all async libraries, its main purpose is to help you write programs that
do multiple things at the same time with parallelized I/O. A web spider that
wants to fetch lots of pages in parallel, a web server that needs to juggle
lots of downloads and websocket connections simultaneously, a process
supervisor monitoring multiple subprocesses... that sort of thing. Compared to
other libraries, Trio attempts to distinguish itself with an obsessive focus
on usability and correctness. Concurrency is complicated; we try to make it
easy to get things right."

LICENCES="
    || ( Apache-2.0 MIT )
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/attrs[>=20.1.0][python_abis:*(-)?]
        dev-python/cffi[>=1.14][python_abis:*(-)?]
        dev-python/idna[python_abis:*(-)?]
        dev-python/outcome[python_abis:*(-)?]
        dev-python/sniffio[>=1.3.0][python_abis:*(-)?]
        dev-python/sortedcontainers[python_abis:*(-)?]
        python_abis:3.8? ( dev-python/exception[python_abis:3.8] )
        python_abis:3.9? ( dev-python/exceptiongroup[python_abis:3.9] )
        python_abis:3.10? ( dev-python/exceptiongroup[python_abis:3.10] )
    test:
        dev-python/astor[python_abis:*(-)?]
        dev-python/cryptography[>=41.0.0][python_abis:*(-)?]
        dev-python/pylint[python_abis:*(-)?]
        dev-python/pyopenssl[>=22.0.0][python_abis:*(-)?]
        dev-python/trustme[python_abis:*(-)?]
"

test_one_multibuild() {
    export PYTEST_DISABLE_PLUGIN_AUTOLOAD=1

    esandbox allow_net "inet:127.0.0.1@30000-65535"
    esandbox allow_net --connect "inet:127.0.0.1@30000-65535"
    esandbox allow_net "inet6:::1@30000-65535"
    esandbox allow_net --connect "inet6:::1@30000-65535"

    edo ${PYTHON} -m installer -d pep517_tests_${PN} -p /usr/$(exhost --target) dist/*.whl

    pushd pep517_tests_${PN}/usr/$(exhost --target)/lib/python$(python_get_abi)/site-packages
    PYTHONPATH="${PWD}" edo ${PYTHON} -m pytest \
        -p trio._tests.pytest_plugin \
        -m "not redistributors_should_skip"  \
        --ignore trio/_tests/test_highlevel_open_tcp_listeners.py \
        --ignore trio/_tests/test_highlevel_open_unix_stream.py \
        --ignore trio/_tests/test_socket.py \
        -k "not test_send_to_closed_local_port and \
            not test_open_stream_to_socket_listener and \
            not test_local_address_real and \
            not test_del and \
            not test_process and \
            not test_lint_failure and not test_run_black and \
            not test_run_ruff"

    esandbox disallow_net --connect "inet6:::1@30000-65535"
    esandbox disallow_net "inet6:::1@30000-65535"
    esandbox disallow_net --connect "inet:127.0.0.1@30000-65535"
    esandbox disallow_net "inet:127.0.0.1@30000-65535"
}

