# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=encode ]
require py-pep517 [ backend=hatchling test=pytest entrypoints=[ ${PN} ] ]

SUMMARY="A lightning-fast ASGI server implementation"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/click[>=7.0][python_abis:*(-)?]
        dev-python/h11[>=0.8][python_abis:*(-)?]
        python_abis:3.8? ( dev-python/typing-extensions[>=4.0][python_abis:3.8] )
        python_abis:3.9? ( dev-python/typing-extensions[>=4.0][python_abis:3.9] )
        python_abis:3.10? ( dev-python/typing-extensions[>=4.0][python_abis:3.10] )
    test:
        dev-python/a2wsgi[>=1.7.0][python_abis:*(-)?]
        dev-python/cryptography[python_abis:*(-)?]
        dev-python/httptools[>=0.5.0][python_abis:*(-)?]
        dev-python/httpx[>=0.18.2][python_abis:*(-)?]
        dev-python/pytest-mock[python_abis:*(-)?]
        dev-python/python-dotenv[>=0.13][python_abis:*(-)?]
        dev-python/PyYAML[>=5.1][python_abis:*(-)?]
        dev-python/trustme[python_abis:*(-)?]
        dev-python/watchfiles[>=0.13][python_abis:*(-)?]
        dev-python/websockets[>=10.4][python_abis:*(-)?]
        dev-python/wsproto[python_abis:*(-)?]
"

PYTEST_SKIP=(
    # The tests below bind to 0.0.0.0 or complain about AF_UNIX path too long
    test_run
    test_bind_unix_socket_works_with_reload_or_workers
)

test_one_multibuild() {
    esandbox allow_net 'unix:/tmp/p-*'

    py-pep517_test_one_multibuild

    esandbox disallow_net 'unix:/tmp/p-*'
}

